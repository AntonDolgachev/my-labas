﻿// Laba6.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
// -----------------------------------------------------------------------------------------------------
// *****************************************************************************************************
//

#include <iostream>
#include <string>
int main()
{
    const int n = 3;
    int a[3][3];
    bool flagIfChange = true;
    int resultMultiply;

    std::cout << "Input a[n][n]:\n";

    for (int i = 0; i < n; i++) // вводим элементы матрицы
    {
        std::cout << "row:" << i+1 << "\n";
        for (int j = 0; j < n; j++)
        {
            std::cin >> a[i][j];
        }
    }
    std::cout << "Input result a[n][n]:\n";

    for (int i = 0; i < n; i++)

    {
        for (int j = 0; j < n; j++)   // выводим элементы матрицы 
        {
            std::cout << a[i][j] << " ";
        }
        std::cout << std::endl;
    }
    
    
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (a[i][i] < a[i][j]) 
            {
                flagIfChange = false;
                std::cout << "Error a[" << i << "][" << i << "] < a[" << i << "][" << j << "]\n";
                return 0;
            }
        }
    }

    if (flagIfChange == true)
    {
        resultMultiply = a[0][0];
        for (int i = 1; i < n; i++)
        {
            resultMultiply = resultMultiply * a[i][i];
        }
        std::cout << "Result multiply a[i][i] = " << resultMultiply << " \n";

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                int x = a[i][j];
                bool flagIfZeroExsist = false;
                
                do
                {
                    if (x % 10 ==0)
                    {
                        flagIfZeroExsist = true;
                    }
                    x = x / 10;
                } while (x > 0 && flagIfZeroExsist == false);

                if (flagIfZeroExsist == true) 
                {
                    a[i][j] = resultMultiply;
                }
            }
        }
    }

    std::cout << "Total result a[n][n]:\n";

    for (int i = 0; i < n; i++)

    {
        for (int j = 0; j < n; j++)   // выводим элементы матрицы 
        {
            std::cout << a[i][j] << " ";
        }
        std::cout << std::endl;
    }
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
